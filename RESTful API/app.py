from flask import Flask, jsonify, make_response
from flask_restful import Resource, Api, reqparse
from keras.models import load_model
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

import re
import tensorflow as tf
import pickle


app = Flask(__name__)
api = Api(app)

classes = ['Non-Offensive', 'Offensive']
model = load_model('Model-Iterasi-2.h5')
graph = tf.get_default_graph()


class Predict(Resource):
    def preprocessing(self, text):
        stopword = StopWordRemoverFactory().create_stop_word_remover()
        stemmer = StemmerFactory().create_stemmer()
        text = text.lower()
        text = re.sub(r" +", " ", text)
        text = re.sub(r"http\S+", "", text)
        text = re.sub(r"[0-9]+", "", text)
        text = re.sub(r"[^\w\s]", " ", text)
        text = stopword.remove(text)
        text = stemmer.stem(text)
        return text

    def embedding(self, texts, maxlen=40):
        with open('tokenizer.pickle', 'rb') as handle:
            tokenizer = pickle.load(handle)    
        sequences = tokenizer.texts_to_sequences(texts)
        data = pad_sequences(sequences, maxlen=maxlen)
        return data

    def get(self, text):
        prep_text = self.preprocessing(text)
        embedded_text = self.embedding([prep_text])
        with graph.as_default():
            result = {
                "text": str(text),
                "class": str(classes[model.predict_classes(embedded_text)[0][0]]),
                "confidence": str(model.predict(embedded_text)[0][0])
            }
        return result, 200 if result else 404


api.add_resource(Predict, "/predict/<string:text>")
if __name__ == '__main__':
    app.run(debug=True)
